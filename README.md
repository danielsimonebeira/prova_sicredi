# Automação de teste desafio sicredi


#### Visão Geral

O projeto possui dois objetivos:

 * 1º objetivo: Testar a api de simulacao de credito via o framework rest assured e o cucumber
 * 2º objetivo: Testar o site grocerycrud via o framework selenium webdriver e o cucumber
 
 
 
 
#### Requisitos
 
  * [Intellij](https://www.jetbrains.com/pt-br/idea/download/#section=linux) (opcional)
  * [Java 8](https://www.oracle.com/br/java/technologies/javase/javase-jdk8-downloads.html)
  * [Maven](https://maven.apache.org/install.html) 

#### Pré-requisitos para o projeto
  * Possuir a API de simulação de crédito na maquina ou em um servidor para executar os testes.

#### Configurações

O projeto possui properties para ajuste da url da api na pasta resources
  
#### Build

Na pasta do projeto digitar no terminal o comando:

```
mvn clean install
```

Após rodar os testes é possível visualisar pelo report no caminho ```target/cucumbe-reports.html```
  
#### Próximos Passos
