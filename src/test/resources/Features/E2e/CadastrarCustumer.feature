Feature: Validar cadastro dos dados de clientes

  Description: Validar cadastro de cliente do site grocerycrud

  Scenario: Validar consulta geral de simulacoes
    Given selecione combo de selecao "Bootstrap V4 Theme"
    And clique no botao "Add Record"
    And no formulario insira o nome "Teste Sicredi" no fomulario
    And o sobrenome "Teste"
    And o nome de contato "seu nome"
    And o telefone "51 9999-9999"
    And a linha de endereco um "Av Assis Brasil, 3970"
    And a linha de endereco dois "Torre D"
    And a cidade "Porto Alegre"
    And o estado "RS"
    And a caixa postal "9100-000"
    And o pais "Brasil"
    And o numero do funcionario do representante de vendas "Fixter"
    And o limite de credito "200"
    When clicar no botao "Save"
    Then Sistema salvar apresentando mensagem de retorno "Your data has been successfully stored into the database. Edit Record or Go back to list"