package Page;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class CadastroPage {
    protected WebDriver driver;

    private final By selectComboTheme = By.id("switch-version-select");
    private final By botaoAddRecord = By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a");
    private final By campoTextoCustumerName = By.id("field-customerName");
    private final By campoTextoContactLastName = By.id("field-contactLastName");
    private final By campoTextoContactFirstName = By.id("field-contactFirstName");
    private final By campoTextoPhone = By.id("field-phone");
    private final By campoTextoAddressLineUm = By.id("field-addressLine1");
    private final By campoTextoAddressLineDois = By.id("field-addressLine2");
    private final By campoTextoCity = By.id("field-city");
    private final By campoTextoState = By.id("field-state");
    private final By campoTextoPostalCode = By.id("field-postalCode");
    private final By campoTextoCountry = By.id("field-country");
    private final By campoTextoSalesRepEmployeeNumber = By.id("field-salesRepEmployeeNumber");
    private final By campoTextoCreditLimit = By.id("field-salesRepEmployeeNumber");
    private final By botaoSave = By.className("btn-success");
    private final By msgSucesso = By.id("report-success");

    public CadastroPage(WebDriver driver) {
        this.driver = driver;

        PageFactory.initElements(driver, this);
        if (!driver.getTitle().equals("")) {
            throw new IllegalStateException("Está não é a pagina grocerycrud. A pagina corrente é " + driver.getCurrentUrl());
        }
    }

    public void selecionaComboTheme(String nomeCombo) {
        Select select = new Select(driver.findElement(selectComboTheme));
        select.selectByVisibleText(nomeCombo);

    }

    public void clicarBotao(@NotNull String nomeBotao) {
        WebDriverWait espera = new WebDriverWait(driver, 60);
        if (nomeBotao.equals("Add Record")) {
            espera.until(ExpectedConditions.visibilityOfElementLocated(botaoAddRecord));
            driver.findElement(botaoAddRecord).isDisplayed();
            String confereNomeBotao = driver.findElement(botaoAddRecord).getText();
            Assert.assertEquals(confereNomeBotao.trim(), nomeBotao);
            driver.findElement(botaoAddRecord).click();
        } else if (nomeBotao.equals("Save")) {
            driver.findElement(botaoSave).isDisplayed();
            String confereNomeBotao = driver.findElement(botaoSave).getText();
            Assert.assertEquals(confereNomeBotao.trim(), nomeBotao);
            driver.findElement(botaoSave).click();
        } else {
            System.out.println("Botão não mapeado!");
        }
    }

    public void digitaCampoNomeFormulario(String textoNome) {
        new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(campoTextoCustumerName));
        driver.findElement(campoTextoCustumerName).sendKeys(Keys.chord(textoNome));

    }

    public void digitaCampoSobrenomeFormulario(String textoSobrenome) {
        driver.findElement(campoTextoContactLastName).sendKeys(Keys.chord(textoSobrenome));

    }

    public void digitaCampoContatoFormulario(String textoContato) {
        driver.findElement(campoTextoContactFirstName).sendKeys(Keys.chord(textoContato));

    }

    public void digitaCampoFoneFormulario(String nuFone) {
        driver.findElement(campoTextoPhone).sendKeys(Keys.chord(nuFone));

    }

    public void digitaCampoEnderecoUmFormulario(String textoEnderecoUm) {
        driver.findElement(campoTextoAddressLineUm).sendKeys(Keys.chord(textoEnderecoUm));

    }

    public void digitaCampoEnderecoDoisFormulario(String textoEnderecoDois) {
        driver.findElement(campoTextoAddressLineDois).sendKeys(Keys.chord(textoEnderecoDois));

    }

    public void digitaCampoCidadesFormulario(String textoCidade) {
        driver.findElement(campoTextoCity).sendKeys(Keys.chord(textoCidade));

    }

    public void digitaCampoEstadoFormulario(String textoEstado) {
        driver.findElement(campoTextoState).sendKeys(Keys.chord(textoEstado));

    }

    public void digitaCampoCaixaPostalFormulario(String textoCaixaPostal) {
        driver.findElement(campoTextoPostalCode).sendKeys(Keys.chord(textoCaixaPostal));

    }

    public void digitaCampoPaisFormulario(String textoPais) {
        driver.findElement(campoTextoCountry).sendKeys(Keys.chord(textoPais));

    }

    public void digitaCampoRepresentanteVendasFormulario(String nuRepresentanteVendas) {
        driver.findElement(campoTextoSalesRepEmployeeNumber).sendKeys(Keys.chord(nuRepresentanteVendas));

    }

    public void digitaCampoCreditoFormulario(String textoCredito) {
        driver.findElement(campoTextoCreditLimit).sendKeys(Keys.chord(textoCredito));

    }

    public void validarMensagemSucesso(String textoMsgEsperado) {
        new WebDriverWait(driver, 60).until(ExpectedConditions.visibilityOfElementLocated(msgSucesso));
        WebElement elementCampoMsg = driver.findElement(msgSucesso);
        List<WebElement> elementTagMsg = elementCampoMsg.findElements(By.tagName("p"));
        for (WebElement msgRecebido: elementTagMsg) {
            Assert.assertEquals(msgRecebido.getText().trim(), textoMsgEsperado);

        }
    }
}
