package StepDefinitions;

import DataProviders.ConfigFileReader;
import Page.CadastroPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class CadastrarCustumer {
    String url;
    WebDriver driver = null;
    ConfigFileReader configFileReader;
    CadastroPage cadastroPage;

    @Before
    public void antesDeIniciar() {
        configFileReader = new ConfigFileReader();
        url = configFileReader.getApplicationUrl("url.e2e");
        System.setProperty("webdriver.chrome.driver", configFileReader.getDriverPath());

    }

    @After
    public void finalizar() {
        if (driver != null) {
            driver.quit();
        }
    }


    @Given("selecione combo de selecao {string}")
    public void selecione_combo_de_selecao(String comboTema) {
        ligarChrome();
        driver.get(url);
        cadastroPage = new CadastroPage(driver);
        cadastroPage.selecionaComboTheme(comboTema);

    }

    @When("clicar no botao {string}")
    public void clicar_no_botao(String botaoSalvar) {
        cadastroPage.clicarBotao(botaoSalvar);

    }

    @And("clique no botao {string}")
    public void clique_no_botao(String botaoAdd) {
        cadastroPage.clicarBotao(botaoAdd);

    }

    @When("no formulario insira o nome {string} no fomulario")
    public void noFormularioInsiraONomeNoFomulario(String nomeTexto) {
        cadastroPage.digitaCampoNomeFormulario(nomeTexto);

    }

    @When("o sobrenome {string}")
    public void o_sobrenome(String sobrenomeTexto) {
        cadastroPage.digitaCampoSobrenomeFormulario(sobrenomeTexto);

    }

    @And("o nome de contato {string}")
    public void o_nome_de_contato(String contatoTexto) {
        cadastroPage.digitaCampoContatoFormulario(contatoTexto);

    }

    @And("o telefone {string}")
    public void o_telefone(String foneNumero) {
        cadastroPage.digitaCampoFoneFormulario(foneNumero);

    }

    @And("a linha de endereco um {string}")
    public void a_linha_de_endereco_um(String enderecoUmTexto) {
        cadastroPage.digitaCampoEnderecoUmFormulario(enderecoUmTexto);

    }

    @And("a linha de endereco dois {string}")
    public void a_linha_de_endereco_dois(String enderecoDoisTexto) {
        cadastroPage.digitaCampoEnderecoDoisFormulario(enderecoDoisTexto);
    }

    @And("a cidade {string}")
    public void a_cidade(String cidadeTexto) {
        cadastroPage.digitaCampoCidadesFormulario(cidadeTexto);

    }

    @And("o estado {string}")
    public void o_estado(String estadoTexto) {
        cadastroPage.digitaCampoEstadoFormulario(estadoTexto);

    }

    @And("a caixa postal {string}")
    public void a_caixa_postal(String caixaPostalTexto) {
        cadastroPage.digitaCampoCaixaPostalFormulario(caixaPostalTexto);

    }

    @And("o pais {string}")
    public void o_pais(String paisTexto) {
        cadastroPage.digitaCampoPaisFormulario(paisTexto);

    }

    @And("o numero do funcionario do representante de vendas {string}")
    public void o_numero_do_funcionario_do_representante_de_vendas(String nuRepresentanteVendasTexto) {
        cadastroPage.digitaCampoRepresentanteVendasFormulario(nuRepresentanteVendasTexto);


    }

    @And("o limite de credito {string}")
    public void o_limite_de_credito(String limiteCreditoNumero) {
        cadastroPage.digitaCampoCreditoFormulario(limiteCreditoNumero);

    }

    @Then("Sistema salvar apresentando mensagem de retorno {string}")
    public void sistema_salvar_apresentando_mensagem_de_retorno(String msgSucesso) {
        cadastroPage.validarMensagemSucesso(msgSucesso);

    }

    public void ligarChrome() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }
}
